#!/usr/bin/env python3
""" Adds the word given on the command line to words.txt """
import sys
import re

import wordmanager

if len(sys.argv) < 2:
    print("No word specified", file=sys.stderr)
    print("Usage: ", sys.argv[0], "[word]")
    quit(1)

WORD = sys.argv[1]

if len(WORD) > 8:
    print("Word must be only 8 characters long", file=sys.stderr)
    quit(2)

if not re.match(r"^[A-Za-z0-9]*$", WORD):
    print("Word must only have letters or numbers", file=sys.stderr)
    quit(3)

try:
    wordmanager.add_word("words.txt", WORD)
except wordmanager.WordExistsError:
    print("Word already exists", file=sys.stderr)
    quit(4)
