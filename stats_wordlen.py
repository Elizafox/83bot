#!/usr/bin/env python3
""" Prints statistics about the word corpus """
import wordmanager

wordmanager.word_len_stats("words.txt")
