#!/usr/bin/env python3
#pylint: disable=invalid-name
# Copyright (c) 2017 Lee Starnes <lee@canned-death.us>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

""" Generates 8.3 filenames and posts them to a Mastodon-compatible instance """

from configparser import ConfigParser
from mastodon import Mastodon
from eightdotthree import Generator

#pylint: enable=invalid-name

CONFIGFILE = 'config.ini'

def read_config():
    """ Read the configuration file and return the parsed version """
    config = ConfigParser()
    config.read(CONFIGFILE)
    return config

def write_config(config):
    """ Save config to the configuration file """
    with open(CONFIGFILE, "w") as configfile:
        config.write(configfile)

def register_if_needed(config):
    """ If needed, register with a new app with the name "83bot" """
    if 'client_secret' in config['bot'] and 'client_id' in config['bot']:
        return
    client_id, client_secret = Mastodon.create_app("83bot", api_base_url=config['bot']['instance'])
    config['bot']['client_id'] = client_id
    config['bot']['client_secret'] = client_secret
    write_config(config)

def login_if_needed(config):
    """ Log into an instance and save the access_token if there isn't one set already """
    if 'access_token' in config['bot']:
        return
    client = Mastodon(
        client_id=config['bot']['client_id'],
        client_secret=config['bot']['client_secret'],
        api_base_url=config['bot']['instance']
    )
    access_token = client.log_in(config['bot']['user'], config['bot']['password'])
    config['bot']['access_token'] = access_token
    write_config(config)

def run():
    """ Connect to the instance, generate an 8.3 filename, and post it """
    config = read_config()
    register_if_needed(config)
    login_if_needed(config)
    client = Mastodon(
        client_id=config['bot']['client_id'],
        client_secret=config['bot']['client_secret'],
        access_token=config['bot']['access_token'],
        api_base_url=config['bot']['instance']
    )

    gen = Generator()
    filename = gen.generate()
    client.status_post(filename, visibility='unlisted', spoiler_text="possibly nsfw (randomly generated)")

if __name__ == "__main__":
    run()
