#!/usr/bin/env python3
# Copyright (c) 2017 Lee Starnes <lee@canned-death.us>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
""" Generates random 8.3-formatted filenames. """

import random
import string
import warnings

# Monkey patch rng to use system rng
random.random = random.SystemRandom().random


class Generator:
    """ Generates random 8.3 filenames """

    singlechars = string.digits

    def __init__(self, wordfile="words.txt", extfile="exts.txt", badfile="bad.txt"):
        self.word_slots = self.read_word_slots(wordfile)
        self.extlist = self.read_wordlist(extfile, 3)
        self.badwords = self.read_wordlist(badfile, 12)

    def __iter__(self):
        return self

    def __next__(self):
        return self.generate_clean()

    @staticmethod
    def read_word_slots(filename):
        """ Read words into slots """
        slots = [[], [], [], [], [], [], [], []]
        with open(filename, "r") as wordfile:
            for word in wordfile:
                word = word.rstrip().upper()
                slot = len(word)
                assert slot <= 8, "Word {} was too long".format(word)

                slots[slot - 1].append(word)

            # Add ancillary characters to slot 1
            slots[0].extend(Generator.singlechars)

        return slots

    @staticmethod
    def read_wordlist(filename, max_len):
        """ Read a file and return a list of words. Skips words longer than max_len """
        wordlist = []
        with open(filename, "r") as wordfile:
            for word in wordfile:
                word = word.strip().upper()
                if len(word) > max_len:
                    warnings.warn("word {} exceeds max length {}".format(word, max_len))
                    continue
                
                wordlist.append(word)

        return wordlist

    def choose_word(self, slot):
        return random.choice(self.word_slots[slot])

    def generate(self):
        """ Generate an 8.3 filename """
        filename = ""
        if random.randint(0, 5) == 5:
            # Random chance to use a length besides 8
            length = random.randint(6, 11)
        else:
            length = 8
        minslot = 1
        while len(filename) < length:
            maxslot = length - len(filename) - 1
            if maxslot > 7:
                # We only have slots 0-7
                maxslot = 7
            elif length - len(filename) == 1:
                # Set minslot to 0 to allow single chars in this case
                minslot = 0
            slot = random.randint(minslot, maxslot)
            filename += self.choose_word(slot)

        if len(filename) == 6:
            return "{}~1.{}".format(filename, random.choice(self.extlist))
        elif len(filename) == 7:
            return "{}~1.{}".format(filename[:6], random.choice(self.extlist))
        elif len(filename) == 8:
            return "{}.{}".format(filename, random.choice(self.extlist))
        elif len(filename) in (9, 10):
            # Corner case: this results in extensions with only numbers usually...
            return "{}.{}".format(filename[:8], random.choice(self.extlist))
        else:
            return "{}.{}".format(filename[:8], filename[8:])

    def generate_clean(self):
        """ Generate an 8.3 filename that does not contain bad words """
        filename = None
        bad = True
        while bad:
            bad = False
            filename = self.generate()
            filename_check = filename.replace(".", "")
            for badword in self.badwords:
                if badword in filename_check:
                    print("Found badword '{}' in '{}'".format(badword, filename))
                    bad = True
                    break
        return filename

def main():
    """ Main entry point """
    import sys
    gen = Generator()
    count = 1
    if len(sys.argv) > 1:
        count = int(sys.argv[1])
    for _ in range(count):
        word = gen.generate_clean()
        print(word)

if __name__ == '__main__':
    main()
