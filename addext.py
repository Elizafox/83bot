#!/usr/bin/env python3
""" Adds the 3-character extension given on the command line to exts.txt """
import sys
import re

import wordmanager

if len(sys.argv) < 2:
    print("No extension specified", file=sys.stderr)
    print("Usage: ", sys.argv[0], "[extension]")
    quit(1)

EXTENSION = sys.argv[1]

if len(EXTENSION) != 3:
    print("Extension must be exactly 3 characters long", file=sys.stderr)
    quit(2)

if not re.match(r"^[A-Za-z0-9]*$", EXTENSION):
    print("Extension must only have letters or numbers", file=sys.stderr)
    quit(3)

try:
    wordmanager.add_word("exts.txt", EXTENSION)
except wordmanager.WordExistsError:
    print("Word already exists", file=sys.stderr)
    quit(4)
