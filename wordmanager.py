#!/usr/bin/env python3
""" Functions for manipulating the word and extension corpuses """
import fcntl

from collections import defaultdict

class WordError(Exception):
    """ Generic word error """
    pass


class WordExistsError(WordError):
    """ Word already exists in corpus """
    pass


class WordNotFoundError(WordError):
    """ Word not found in corpus """
    pass


def add_word(filename, word):
    """ Adds a word to the corpus file """
    if not word.endswith("\n"):
        word += "\n"

    lowerword = word.lower()

    with open(filename, "r+") as corpus:
        fcntl.flock(corpus, fcntl.LOCK_EX)
        words = corpus.readlines()

        if any(lowerword == x.lower() for x in words):
            raise WordExistsError()

        words.append(word)
        words.sort(key=str.lower)

        corpus.seek(0)
        corpus.writelines(words)
        corpus.truncate()


def del_word(filename, word):
    """ Deletes a word from a corpus file """
    if not word.endswith("\n"):
        word += "\n"

    lowerword = word.lower()

    with open(filename, "r+") as corpus:
        fcntl.flock(corpus, fcntl.LOCK_EX)

        words = corpus.readlines()
        newwords = [x for x in words if lowerword != x.lower()]

        if len(newwords) == len(words):
            raise WordNotFoundError()

        corpus.seek(0)
        corpus.writelines(newwords)
        corpus.truncate()


def word_len_stats(filename):
    """ Print word length stats about a given file """

    lengths = defaultdict(int)

    with open(filename, "r") as corpus:
        fcntl.flock(corpus, fcntl.LOCK_EX)

        for word in corpus:
            word = word.strip()
            
            lengths[len(word)] += 1

    print("Length statistics for corpus {}".format(filename))
    for index in range(1, max(lengths.keys()) + 1):
        print("{:<4}{}".format(str(index) + ":", lengths[index]))
