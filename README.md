# 83bot #
It generates lewd 8.3 format filenames. Not much else to it.

## Disclaimer ##
This project is not connected to the Interlinked Foundation in any way.

## Setup ##
1. Register an account on a Mastodon instance. Other instances that support the Mastodon client API will work too, like Pleroma.
2. Register the app. In Mastodon, go to Preferences > Development. Leave the redrect URI as `urn:ietf:wg:oauth:2.0:oob`. 
   Make sure it has the `read` and `write` scopes. Make note of the `client_id`, `client_secret`, and `access_token`.
3. Copy `config.ini.example` to `config.ini` then change the fields as follows:
   * `instance` is your instance's base URL
   * `user` is the email address you registered the bot with.
   * `password` is the account's password.
   * `client_id`, `client_secret`, and `access_token` are what you got in step 2.
5. Create a `venv` and install the required dependencies
```
$ python3 -m venv ./venv
$ ./venv/bin/pip install -r requirements.txt
```
6. Run it!
```
$ ./venv/bin/python3 83bot.py
```

## Notes ##
83bot.py just posts once then exits. If you want to have it post regularly, set up a cron job or equivalent to run it as needed.

Posts are unlisted, so its posts won't appear in the federated or local timelines. The bot will use spoilers at present unconditionally.

83bot will update its config.ini if neeed to update its `access_token` if it expired.

## Managing words ##
Use the scripts `addword.py` and `delword.py` to add or delete words in the main word list, or just edit `words.txt` directly. 

For extensions, use `addext.py` or `delext.py` or edit `exts.txt` directly. Extensions should be exactly 3 characters long.

To prevent certain words from appearing in the output due to unfortunate word combinations, add those words to `bad.txt`.

## Enhancement suggestions ##
Message luciferMysticus@mst3k.interlinked.me or Elizafox@mst3k.interlinked.me
